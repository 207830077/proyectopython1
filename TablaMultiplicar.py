

#opcion = 1

opcion = 's'


#while opcion!=0:

while ((opcion!='n') or (opcion!='N')):
    mensaje_Entrada = "programa de generacion de tablas de multiplicar".capitalize()
    print(mensaje_Entrada.center(70,"="))

    tab_Inicio = int(input(chr(27) + "[1;35m" + "Ingrese la tabla inicial: "))  #Cambio de color con chr(27)
    tab_Final = int(input("Ingrese la tabla final: "))

    while tab_Final < tab_Inicio:
        print("La tabla de inicio debe ser menor que la tabla final ")
        tab_Inicio = int(input("Ingrese la tabla inicial: "))
        tab_Final = int(input("Ingrese la tabla final: "))


    rango_Inicio = int(input("Ingrese el rango inicial: "))
    rango_Final = int(input("Ingrese el rango final: "))


    while  rango_Final < rango_Inicio:
        print("El rango inicial debe ser menor que el rango final ")
        rango_Inicio = int(input("Ingrese el rango inicial: "))
        rango_Final = int(input("Ingrese el rango final: "))


    while tab_Final > tab_Inicio:
        for i in range(tab_Inicio,tab_Final+1):
            if i == 4:
                print("La tabla {0} no la imprimo porque no quiero ".format(i))
                continue
            for j in range(rango_Inicio,rango_Final+1):
                if j == 5:
                    print("Mas de 5 no quiero ")
                    break
                resultado = i * j
                #print("Multiplicar ", i , " * ", ii ,"es igual a: ",resultado)
                print("Multiplicar %d * %d es igual a: %d" % (i,j,resultado))

                tab_Inicio=+1

    opcion = input("Desea ejecutar nuevamente el proceso: \n"
                            "n: Salir \n"
                            "s: Continuar \n")


    if ((opcion!='n') or (opcion!='N')):
        break
else:
    print("Gracias por jugar ")


