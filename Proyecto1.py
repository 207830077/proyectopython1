
opcion = 1

while opcion!=0:

    mensaje_Entrada = "programa de generacion de tablas de multiplicar".capitalize()
    print(mensaje_Entrada.center(70,"="))

    tab_Inicio = int(input(chr(27) + "[1;35m" + ("Ingrese la tabla inicial: ")))
    tab_Final = int(input("Ingrese la tabla final: "))




    while tab_Final < tab_Inicio:
        print("La tabla de inicio debe ser menor que la tabla final ")
        tab_Inicio = int(input("Ingrese la tabla inicial: "))
        tab_Final = int(input("Ingrese la tabla final: "))


    rango_Inicio = int(input("Ingrese el rango inicial: "))
    rango_Final = int(input("Ingrese el rango final: "))


    while rango_Final < rango_Inicio:
        print("El rango inicial debe ser menor que el rango final ")
        rango_Inicio = int(input("Ingrese el rango inicial: "))
        rango_Final = int(input("Ingrese el rango final: "))



    for i in range(tab_Inicio,tab_Final+1):

        if i == 4:
            print("La tabla {0} no la imprimo porque no quiero ".format(i))
            continue

        for ii in range(rango_Inicio,rango_Final+1):
            if ii == 5:
                print("Mas de 5 no quiero ")
                break

            resultado = i * ii
            #print("Multiplicar ", i , " * ", ii ,"es igual a: ",resultado)
            print("Multiplicar %d * %d es igual a: %d" % (i,ii,resultado))

    opcion = int(input("Desea ejecutar nuevamente el proceso: \n"
                       "0 : Salir \n"
                       "1 : Continuar \n"))
else:
    print("Gracias por jugar ")


